# F1 Champions

Dieses projekt gibt einem den Fahrer-Weltmeister der FIA Formel 1 in desem Jahrtausend wieder.
Dafür einfach Das Jahr der Funktion mitgeben, und man bekommt die Initialen vom Vornamen und den Nachnamen.
Die Funktion findet man in F1Champs.py
----

F1Champs.py:

Die Funktion vergleicht in einem (mit einen Dictionalry selbsterstellten) Switch case. den eingegeben Wert, und gibt(returnt) dann den in Dictionary definierten String aus.
----

Test_F1Champ:

Hier wird die Funktion von F1Champs importiert und getestet.
Dazu gibt es 2 Testszenarien:
1.Was passiert wenn man einen gültiges Jahr übergibt, In dem Fall wurde mit dem Jahr 2009 getestet, und es müsste J.Button(Jenson Button) zurückgegeben werden
2.Was passiert, wenn man ein falsches Jahr übergibt. In dem Fall wurde mit dem Jahr 2020 getestet, dass noch keien Weltmeister hat. Ob es für das Jahr einen gibt steht wegen des Coronavirus noch in den Sternen.
----

Pipeline und CI:

die Datei.gitlab-ci.yml wird bei jedem commit automatisch bei Github ausgeführt. Die checkt vor dem Testen ein paar Sachen wie User, und Verzeichnis und testet daraufhin das Progrmamm, und führt es danach normal aus.
Fällt der Test durch, oder givt es ein Fehler beim Ausführen, geht auch die Pipeline in den Status fail.

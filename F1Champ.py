#F1 Weltmeister 2000 bis 2019
#Jahr=input("Bitte Jahr eingeben, um den F1 Weltmeister von 2000 bis 2019 zu erfahren: ")
#print(Jahr)




def Weltmeister(Jahr):
    switcher = {
        "2000": "M.Schumacher",
        "2001": "M.Schumacher",
        "2002": "M.Schumacher",
        "2003": "M.Schumacher",
        "2004": "M.Schumacher",
        "2005": "F.Alonso",
        "2006": "F.Alonso",
        "2007": "K.Raikkonen",
        "2008": "L.Hamilton",
        "2009": "J.Button",
        "2010": "S.Vettel",
        "2011": "S.Vettel",
        "2012": "S.Vettel",
        "2013": "S.Vettel",
        "2014": "L.Hamilton",
        "2015": "L.Hamilton",
        "2016": "N.Rosberg",
        "2017": "L.Hamilton",
        "2018": "L.Hamilton",
        "2019": "L.Hamilton",
        
    }
    result= switcher.get(Jahr, "Falsches Jahr")
    print (result)
    return result
    
#Weltmeister(Jahr)
